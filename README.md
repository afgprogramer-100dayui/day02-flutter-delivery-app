## Development Setup

Clone the repository and run the following commands:

```
flutter pub get
flutter run
```

## Source

[Afgprogrammer Youtube Channel](https://youtube.com/afgprogrammer)

## Screenshot

<img src="assets/screenshot/one.png" height="500em" /> &nbsp; <img src="assets/screenshot/two.png" height="500em" /> &nbsp; <img src="assets/screenshot/three.png" height="500em" />
